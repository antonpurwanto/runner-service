package com.example.app.util;

import java.io.FileWriter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.example.app.entity.Member;

public class GenerateCsv {

	private static Logger log = LoggerFactory.getLogger(GeneratePdf.class);

	public static void memberReport(List<Member> members, String filename ) {
		try {
			FileWriter fw = new FileWriter(filename);
			for (Member mb : members) {

                fw.append(mb.getId());
                fw.append(',');
                fw.append(mb.getName());
                fw.append(',');
                fw.append(mb.getEmail());
                fw.append('\n');

			}

			fw.flush();
			fw.close();

			log.info("CSV File is created successfully.");
		} catch (Exception e) {
			log.info("exception {}",e.getMessage());
		}

	}

}
