package com.example.app.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.app.entity.Member;
import com.example.app.entity.dao.MemberDao;
import com.example.app.util.GeneratePdf;

@RestController
@RequestMapping("/pdf")
public class ReportController {

	@Autowired
	MemberDao memberDao;

	@GetMapping(value = "/member", produces = MediaType.APPLICATION_PDF_VALUE)
	public ResponseEntity<InputStreamResource> memberReport() throws IOException {

		List<Member> mb = (List<Member>) memberDao.findAll();

		ByteArrayInputStream bis = GeneratePdf.memberReport(mb);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "inline; filename=member.pdf");

		return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
				.body(new InputStreamResource(bis));
	}
}