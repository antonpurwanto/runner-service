package com.example.app.entity.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.example.app.entity.Member;

public interface MemberDao extends PagingAndSortingRepository<Member, String> {
}
