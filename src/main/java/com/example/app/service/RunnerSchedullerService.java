package com.example.app.service;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.SystemPropertyUtils;

import com.example.app.entity.Member;
import com.example.app.entity.dao.MemberDao;
import com.example.app.util.CSV;
import com.example.app.util.GenerateCsv;
import com.example.app.util.GeneratePdf;
import com.itextpdf.text.pdf.codec.Base64.InputStream;
import com.itextpdf.text.pdf.codec.Base64.OutputStream;

@Service
@EnableScheduling
public class RunnerSchedullerService {

	private static Logger log = LoggerFactory.getLogger(RunnerSchedullerService.class);

	@Value("${path.file.name}")
	private String pathFileName;

	@Value("${write.file.name}")
	private String writeFileName;
	
	@Value("${report.file}")
	private String reportName;

	@Autowired
	MemberDao memberDao;

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(cron = "${cron.job.expression}")
	public void reportCurrentTime() {
		long now = System.currentTimeMillis();
		log.info("Scheduler run on {}", dateFormat.format(new Timestamp(now)));
		// logic here
		/* String pathFile = new FileSystemResource("/member.csv").toString(); */
		String fileName = new ClassPathResource(pathFileName).getPath();

		log.info(fileName);
		List<String[]> data = CSV.read(fileName);
		for (String[] column : data) {

			System.out.println("no = " + column[0]);
			System.out.println("id = " + column[1]);
			System.out.println("name = " + column[2]);
			System.out.println("last name = " + column[3]);
			System.out.println("dob = " + column[4]);
			System.out.println("poin = " + column[5]);
			System.out.println("store = " + column[6]);
			System.out.println("email = " + column[7]);
			System.out.println("phone = " + column[8]);
			System.out.println("Membership = " + column[9]);
			/*
			 * for (String eachdata : dt){ System.out.println(eachdata); }
			 */
		}

	}

	@Scheduled(cron = "${cron.report.expression}")
	public void report() throws IOException {
		long now = System.currentTimeMillis();
		log.info("Scheduler Report run on {}", dateFormat.format(new Timestamp(now)));
		// logic here

		log.info("menjalankan reporting system");
		
		List<Member> mb = (List<Member>) memberDao.findAll();
		GenerateCsv.memberReport(mb, reportName);

		/*List<Member> mb = (List<Member>) memberDao.findAll();

		// ByteArrayInputStream bis = GeneratePdf.memberReport(mb);
		ByteArrayInputStream byteArrayInputStream = GeneratePdf.memberReport(mb);
		String outStr = byteArrayInputStream.toString();

		File newFile = new File(writeFileName);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(newFile);
		} catch (FileNotFoundException e) {
			log.info("file not found system{}", e.getMessage());

		}
		int data;
		while ((data = byteArrayInputStream.read()) != -1) {
			char ch = (char) data;
			fos.write(ch);
		}
		fos.flush();
		fos.close();*/

	}

}
