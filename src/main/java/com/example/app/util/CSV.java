package com.example.app.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.app.service.RunnerSchedullerService;

public class CSV {
	
	private static Logger log = LoggerFactory.getLogger(CSV.class);
	
	public static List<String[]> read(String file) {

		List<String[]> data = new LinkedList<String[]>();

		String line;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] datalines = line.split(",");
				data.add(datalines);
			}

		} catch (FileNotFoundException e) {	
			log.info("file can't found {}", e.getMessage());
		} catch (IOException e) {
			log.info("file can't read  {}", e.getMessage());
		}

		return data;
	}

}
