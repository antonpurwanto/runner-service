package com.example.app.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "member")
public class Member {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	@Getter @Setter
	private String id;
	
	@NotNull
	@Getter @Setter
	@Size(min = 3, max = 25)
	private String nidm;
	
	@NotNull
	@Getter @Setter
	@Size(min = 3, max = 25)
	private String name;
	
	@NotNull
	@Getter @Setter
	@Size(min = 3, max = 25)
	private String lname;
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Getter
	@Setter
	@Column(name = "date_birth", nullable = true)
	private Date dob;
	
	@NotNull
	@Getter @Setter
	@Size(min = 3, max = 25)
	private String byStore;
	
	@NotNull
	@Getter @Setter
	@Size(min = 3, max = 25)
	private String balance;
	
	@Email
    @NotNull
    @NotEmpty
    @Getter @Setter
    @Column(nullable = false, unique = true)
    private String email;
	
	@NotNull
	@NotEmpty
	@Getter
	@Setter
	private String phone;
	
	@NotNull
	@Getter
	@Setter
	@Size(min = 3, max = 10)
	private String membership;
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Getter
	@Setter
	@Column(name = "created_by", nullable = true)
	private Date created;
	
	@NotNull
	@Past
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@Temporal(TemporalType.DATE)
	@Getter
	@Setter
	@Column(name = "last_purchased", nullable = true)
	private Date lastPurchasedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNidm() {
		return nidm;
	}

	public void setNidm(String nidm) {
		this.nidm = nidm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLname() {
		return lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getByStore() {
		return byStore;
	}

	public void setByStore(String byStore) {
		this.byStore = byStore;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastPurchasedDate() {
		return lastPurchasedDate;
	}

	public void setLastPurchasedDate(Date lastPurchasedDate) {
		this.lastPurchasedDate = lastPurchasedDate;
	}
	
}
